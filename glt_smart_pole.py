import time
from subprocess import Popen, PIPE
import artikcloud
import json
from artikcloud.apis.messages_api import MessagesApi
from random import randint
from websocket import create_connection
import thread
from threading import Timer

DEFAULT_CONFIG_PATH = 'config/config.json'

INTERVAL = 10 #in seconds
STREAMING_TIMER = 60.0

class SmartPole:

    sdid = ''
    token = ''
    akc_config = artikcloud.Configuration()
    ARTIK_URI = "wss://api.artik.cloud/v1.1/websocket?ack=true"
    ###ws = websocket.WebSocket()
    ws = create_connection(ARTIK_URI)

    svid = 0
    vtarget = ""
    coms = ""
    timer = 0


    def __init__(self):
        with open(DEFAULT_CONFIG_PATH, 'r') as config_file:
            config = json.load(config_file)['pole']
        self.akc_config.access_token = config['deviceToken']
        self.token = config['deviceToken']
        self.sdid  = config['deviceId']
        REGISTER_JSON = json.dumps({"sdid": self.sdid,
                                    "Authorization": "bearer " + self.token,
                                    "type": "register"
                                    })

        ##self.ws.connect(self.ARTIK_URI)

        try:
            print "starting websocket listner thread"
            thread.start_new_thread(self.process_action, (self.ws, "ws"))
        except:
            print "Error: unable to start thread"

        self.ws.send(REGISTER_JSON)


    def send_message(self,field,val):
        message = artikcloud.Message()
        message.type = "message"
        message.sdid = self.sdid
        message.ts = int(round(time.time() * 1000))
        message.data = {field: val}
        api = MessagesApi()

        response = api.send_message(message)
        #pprint(response)

    ##This function handles a call to stream made by submiting a vtarget
    def process_vtarget(self):
        if(self.svid == 0):
            self.svid = 1
            self.streamproc = Popen(['vlc', '-vvv', '/Users/guymorgenstern/Downloads/2017-07-20_2-05-30.mp4', '--sout', '#transcode{vcodec=h264,vb=1024}:rtp{mux=ts,dst=127.0.0.1,port=6000}'], stdin=PIPE,stdout=PIPE)
            msg_type = 'SVID'
            self.send_message(msg_type,self.svid)
            self.timer = Timer(STREAMING_TIMER, self.time_ends)
            self.timer.start()
    ##This function handles a call to stream made by submiting a vtarget
    def process_svid(self, new_svid):
        print self.streamproc
        print self.svid
        print "kill 0"
        if ((self.svid == 1) and (new_svid == 0)):
            self.svid = 0
            self.streamproc.stdin.write('quit\n')
            msg_type = 'SVID'
            self.send_message(msg_type,self.svid)
        elif ((self.svid == 1) and (new_svid == 1)):
            self.timer.cancel()
            self.timer = Timer(STREAMING_TIMER, self.time_ends)
            self.timer.start()


    def time_ends(self):
        if (self.svid == 1) :
            self.svid = 0
            self.streamproc.stdin.write('quit\n')

    def process_action(self,ws,name):
        print "listener thread started"

        while(True):
            result = ws.recv()

            json_res = json.loads(result)
            if ('data' in json_res and 'actions' in json_res['data'] and
                        json_res['data']['actions'][0]['name'] == 'setBrightness'):
                print "brightness is", json_res['data']['actions'][0]['parameters']['brightness']
            if ('data' in json_res and 'actions' in json_res['data'] and
                        json_res['data']['actions'][0]['name'] == 'setSVID'):
                new_svid = json_res['data']['actions'][0]['parameters']['svid']
                self.process_svid(new_svid)
                print " new svid is", new_svid
            if ('data' in json_res and 'actions' in json_res['data'] and
                        json_res['data']['actions'][0]['name'] == 'setVTARGET'):
                self.vtarget = json_res['data']['actions'][0]['parameters']['vtarget']
                self.process_vtarget()
                print "vtarget is", self.vtarget
            if ('data' in json_res and 'actions' in json_res['data'] and
                        json_res['data']['actions'][0]['name'] == 'setCOMS'):
                self.coms = json_res['data']['actions'][0]['parameters']['coms']
                print "coms is", self.coms

                ####Process SVID



if __name__ == '__main__':

    pole = SmartPole()

    while(True):
        msg_type_int = randint(1, 4)
        if(msg_type_int == 1):
            msg_type = 'UBATT'
        elif(msg_type_int == 2):
            msg_type = 'TBATT'
        elif(msg_type_int == 3):
            msg_type = 'BATTSTAT'
        else:
            msg_type = 'BRGT'

        value = randint(0,100)
        print "sending {} {}".format(msg_type, value)
        pole.send_message(msg_type,value)
        time.sleep(INTERVAL)
